CREATE TABLE users (
    "id" VARCHAR(10) NOT NULL primary key,
    "name" VARCHAR(20) NOT NULL,
    "email" VARCHAR(50) NOT NULL,
    "password" VARCHAR(10) NOT NULL,
    "level" INTEGER  NOT NULL  DEFAULT (1),
    "login" INTEGER  NOT NULL  DEFAULT (0),
    "recommend" INTEGER  NOT NULL  DEFAULT (0)
)