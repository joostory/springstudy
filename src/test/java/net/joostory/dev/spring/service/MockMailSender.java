package net.joostory.dev.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MockMailSender implements MailSender {

	private List<String> requests = new ArrayList<String>();
	
	public List<String> getRequests() {
		return requests;
	}
	
	
	@Override
	public void send(SimpleMailMessage message) throws MailException {
		requests.add(message.getTo()[0]);
	}

	@Override
	public void send(SimpleMailMessage[] messages) throws MailException {
		for(SimpleMailMessage message:messages) {
			requests.add(message.getTo()[0]);
		}
	}

}
