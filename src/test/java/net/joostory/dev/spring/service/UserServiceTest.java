package net.joostory.dev.spring.service;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.joostory.dev.spring.AppContext;
import net.joostory.dev.spring.dao.UserDao;
import net.joostory.dev.spring.domain.Level;
import net.joostory.dev.spring.domain.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("production")
@ContextConfiguration(classes=AppContext.class)
@Transactional
public class UserServiceTest {
	@Autowired
	ApplicationContext context;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserService testUserService;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	PlatformTransactionManager transactionManager;
	
	@Autowired
	MailSender mailSender;
	
	List<User> users;
	
	@Before
	public void setUp() {
		users = Arrays.asList(
					new User("id1", "name1", "name1@joostory.net", "password1", Level.BASIC, UserServiceImpl.MIN_LOGCOUNT_FOR_SILVER - 1, 0),
					new User("id2", "name2", "name2@joostory.net", "password2", Level.BASIC, UserServiceImpl.MIN_LOGCOUNT_FOR_SILVER, 0),
					new User("id3", "name3", "name3@joostory.net", "password3", Level.SILVER, UserServiceImpl.MIN_LOGCOUNT_FOR_SILVER, UserServiceImpl.MIN_RECOMMEND_FOR_GOLD - 1),
					new User("id4", "name4", "name4@joostory.net", "password4", Level.SILVER, UserServiceImpl.MIN_LOGCOUNT_FOR_SILVER, UserServiceImpl.MIN_RECOMMEND_FOR_GOLD),
					new User("id5", "name5", "name5@joostory.net", "password5", Level.GOLD, Integer.MAX_VALUE, Integer.MAX_VALUE),
					new User("id6", "name6", "name6@joostory.net", "password6", Level.SILVER, Integer.MAX_VALUE, 0),
					new User("id7", "name7", "name7@joostory.net", "password7", Level.BASIC, UserServiceImpl.MIN_LOGCOUNT_FOR_SILVER, UserServiceImpl.MIN_RECOMMEND_FOR_GOLD)
				);
	
		userService.deleteAll();
	}
	
	@Autowired DefaultListableBeanFactory bf;
	@Test
	public void beans() {
		System.out.println(bf.getBeanDefinitionNames().length);
		for(String n : bf.getBeanDefinitionNames()) {
			System.out.println(bf.getBean(n).getClass());
		}
	}
	
	@Test
	@DirtiesContext
	public void mockUpgradeLevels() throws Exception {
		UserServiceImpl userServiceImpl = new UserServiceImpl();
		
		UserDao mockUserDao = mock(UserDao.class);
		when(mockUserDao.getAll()).thenReturn(users);
		userServiceImpl.setUserDao(mockUserDao);
		
		MailSender mockMailSender = mock(MailSender.class);
		userServiceImpl.setMailSender(mockMailSender);
		
		userServiceImpl.upgradeLevels();
		verify(mockUserDao, times(3)).update(any(User.class));
		verify(mockUserDao, times(3)).update(any(User.class));
		verify(mockUserDao).update(users.get(1));
		assertThat(users.get(1).getLevel(), is(Level.SILVER));
		verify(mockUserDao).update(users.get(3));
		assertThat(users.get(3).getLevel(), is(Level.GOLD));
		
		ArgumentCaptor<SimpleMailMessage> mailMessageArg = ArgumentCaptor.forClass(SimpleMailMessage.class);
		verify(mockMailSender, times(3)).send(mailMessageArg.capture());
		List<SimpleMailMessage> mailMessages = mailMessageArg.getAllValues();
		assertThat(mailMessages.get(0).getTo()[0], is(users.get(1).getEmail()));
		assertThat(mailMessages.get(1).getTo()[0], is(users.get(3).getEmail()));
	}
	
	private void checkLevelUpgraded(User user, boolean upgraded) {
		User userUpdate = userDao.get(user.getId());
		if (upgraded) {
			assertThat(userUpdate.getLevel(), is(user.getLevel().nextLevel()));
		} else {
			assertThat(userUpdate.getLevel(), is(user.getLevel()));
		}
	}
	
	private void checkLevel(User user, Level expectedLevel) {
		User userUpdate = userDao.get(user.getId());
		assertThat(userUpdate.getLevel(), is(expectedLevel));
	}
	
	@Test
	public void add() {
		User userWithLevel = users.get(4);
		User userWithoutLevel = users.get(1);
		userWithoutLevel.setLevel(null);
		
		userService.add(userWithLevel);
		userService.add(userWithoutLevel);
		
		checkLevel(userDao.get(userWithLevel.getId()), userWithLevel.getLevel());
		checkLevel(userDao.get(userWithoutLevel.getId()), Level.BASIC);
	}
	
	@Test
	public void upgradeAllOrNothing() throws Exception {
		for (User user:users) {
			testUserService.add(user);
		}
		
		try {
			testUserService.upgradeLevels();
			fail("TestUserServiceException expected");
		} catch(TestUserServiceException e) {
		}
		
		checkLevelUpgraded(users.get(1), false);
	}
	
	@Test
	public void advisorAutoProxyCreator() {
		assertThat((Proxy) testUserService, is(java.lang.reflect.Proxy.class));
	}
	
	
	@Test
	public void readOnlyTransactionAttribute() {
		testUserService.getAll();
	}
	
	@Test
	@Transactional(propagation=Propagation.NEVER)
	public void transactionSync() {
		userService.add(users.get(0));
		userService.add(users.get(1));
	}
	
	public static class TestUserService extends UserServiceImpl {

		private String id = "id4";
		
		@Override
		protected void upgradeLevel(User user) {
			if (user.getId().equals(this.id)) {
				throw new TestUserServiceException();
			}
			super.upgradeLevel(user);
		}
		
		@Override
		public List<User> getAll() {
			for(User user: super.getAll()) {
				super.update(user);
			}
			return null;
		}
	}

	static class TestUserServiceException extends RuntimeException {
	}
	
	static class MockUserDao implements UserDao {

		private List<User> users;
		private List<User> updated = new ArrayList<User>();
		
		private MockUserDao(List<User> users) {
			this.users = users;
		}
		
		public List<User> getUpdated() {
			return updated;
		}

		@Override
		public List<User> getAll() {
			return users;
		}
		
		@Override
		public void update(User user) {
			updated.add(user);
		}
		
		@Override
		public void add(User user) {
			throw new UnsupportedOperationException();
		}

		@Override
		public User get(String id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void deleteAll() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getCount() {
			throw new UnsupportedOperationException();
		}
		
	}
}
