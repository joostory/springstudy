package net.joostory.dev.spring.template;

import java.io.IOException;

public interface LineCallback<T> {

	T doSomeThingWithLine(String line, T value) throws IOException;
}
