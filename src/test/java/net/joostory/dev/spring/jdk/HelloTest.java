package net.joostory.dev.spring.jdk;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.lang.reflect.Proxy;

import org.junit.Test;

public class HelloTest {

	@Test
	public void simpleProxy() {
		Hello hello = new HelloTarget();
		assertThat(hello.sayHello("Joo"), is("Hello Joo"));
		assertThat(hello.sayHi("Joo"), is("Hi Joo"));
		assertThat(hello.sayThankYou("Joo"), is("Thank You Joo"));
	}
	
	@Test
	public void uppercaseProxy() {
		Hello hello = (Hello) Proxy.newProxyInstance(
				getClass().getClassLoader(),
				new Class[] { Hello.class },
				new UppercaseHandler(new HelloTarget()));
		assertThat(hello.sayHello("Joo"), is("HELLO JOO"));
		assertThat(hello.sayHi("Joo"), is("HI JOO"));
		assertThat(hello.sayThankYou("Joo"), is("THANK YOU JOO"));
	}
}
