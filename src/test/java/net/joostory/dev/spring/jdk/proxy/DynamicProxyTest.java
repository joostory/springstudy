package net.joostory.dev.spring.jdk.proxy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.lang.reflect.Proxy;

import net.joostory.dev.spring.jdk.Hello;
import net.joostory.dev.spring.jdk.HelloTarget;
import net.joostory.dev.spring.jdk.UppercaseHandler;

import org.junit.Test;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;

public class DynamicProxyTest {

	@Test
	public void simpleProxy() {
		Hello proxiedHello = (Hello) Proxy.newProxyInstance(
				getClass().getClassLoader(),
				new Class[] { Hello.class },
				new UppercaseHandler(new HelloTarget()));
		
		assertThat(proxiedHello.sayHello("Joo"), is("HELLO JOO"));
		assertThat(proxiedHello.sayHi("Joo"), is("HI JOO"));
		assertThat(proxiedHello.sayThankYou("Joo"), is("THANK YOU JOO"));
	}
	
	@Test
	public void proxyFactoryBean() {
		ProxyFactoryBean pfBean = new ProxyFactoryBean();
		pfBean.setTarget(new HelloTarget());
		pfBean.addAdvice(new UppercaseAdvice());
		
		Hello proxiedHello = (Hello) pfBean.getObject();
		assertThat(proxiedHello.sayHello("Joo"), is("HELLO JOO"));
		assertThat(proxiedHello.sayHi("Joo"), is("HI JOO"));
		assertThat(proxiedHello.sayThankYou("Joo"), is("THANK YOU JOO"));
	}
	
	@Test
	public void pointcutAdvisor() {
		ProxyFactoryBean pfBean = new ProxyFactoryBean();
		pfBean.setTarget(new HelloTarget());
		
		NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();
		pointcut.setMappedName("sayH*");
		
		pfBean.addAdvisor(new DefaultPointcutAdvisor(pointcut, new UppercaseAdvice()));
		
		Hello proxiedHello = (Hello) pfBean.getObject();
		assertThat(proxiedHello.sayHello("Joo"), is("HELLO JOO"));
		assertThat(proxiedHello.sayHi("Joo"), is("HI JOO"));
		assertThat(proxiedHello.sayThankYou("Joo"), is("Thank You Joo"));
	}
	
	@Test
	public void classNamePointcutAdvisor() {
		NameMatchMethodPointcut classMethodPointcut = new NameMatchMethodPointcut() {
			public ClassFilter getClassFilter() {
				return new ClassFilter() {

					@Override
					public boolean matches(Class<?> clazz) {
						return clazz.getSimpleName().startsWith("HelloT");
					}
					
				};
			}
		};
		classMethodPointcut.setMappedName("sayH*");
		checkAdviced(new HelloTarget(), classMethodPointcut, true);
		
		class HelloWorld extends HelloTarget {};
		checkAdviced(new HelloWorld(), classMethodPointcut, false);
		
		class HelloToby extends HelloTarget {};
		checkAdviced(new HelloToby(), classMethodPointcut, true);
	}

	private void checkAdviced(Object target, Pointcut pointcut, boolean adviced) {
		ProxyFactoryBean pfBean = new ProxyFactoryBean();
		pfBean.setTarget(target);
		pfBean.addAdvisor(new DefaultPointcutAdvisor(pointcut, new UppercaseAdvice()));
		
		Hello proxiedHello = (Hello) pfBean.getObject();
		if (adviced) {
			assertThat(proxiedHello.sayHello("Joo"), is("HELLO JOO"));
			assertThat(proxiedHello.sayHi("Joo"), is("HI JOO"));
			assertThat(proxiedHello.sayThankYou("Joo"), is("Thank You Joo"));
		} else {
			assertThat(proxiedHello.sayHello("Joo"), is("Hello Joo"));
			assertThat(proxiedHello.sayHi("Joo"), is("Hi Joo"));
			assertThat(proxiedHello.sayThankYou("Joo"), is("Thank You Joo"));
		}
	}
}
