package net.joostory.dev.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import net.joostory.dev.spring.domain.Level;
import net.joostory.dev.spring.domain.User;
import net.joostory.dev.spring.sqlservice.SqlService;

@Repository
public class UserDaoJdbc implements UserDao {

	@Autowired
	private SqlService sqlService;
	
	private JdbcTemplate jdbcTemplate;
	
	private RowMapper<User> userMapper = new RowMapper<User>() {
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getString("id"));
			user.setName(rs.getString("name"));
			user.setEmail(rs.getString("email"));
			user.setPassword(rs.getString("password"));
			user.setLevel(Level.valueOf(rs.getInt("level")));
			user.setLogin(rs.getInt("login"));
			user.setRecommend(rs.getInt("recommend"));
			return user;
		}
	};
	
	public UserDaoJdbc() {}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void add(User user) {
		jdbcTemplate.update(sqlService.getSql("userAdd"),
				user.getId(), user.getName(), user.getEmail(), user.getPassword(), user.getLevel().intValue(), user.getLogin(), user.getRecommend());
	}

	@Override
	public User get(String id) {
		return jdbcTemplate.queryForObject(sqlService.getSql("userGet"), new Object[] { id }, userMapper);
	}

	@Override
	public void deleteAll() {
		jdbcTemplate.update(sqlService.getSql("userDeleteAll"));
	}

	@Override
	public int getCount() {
		return jdbcTemplate.queryForObject(sqlService.getSql("userGetCount"), Integer.class);
	}

	@Override
	public List<User> getAll() {
		return jdbcTemplate.query(sqlService.getSql("userGetAll"), userMapper);
	}

	@Override
	public void update(User user) {
		jdbcTemplate.update(sqlService.getSql("userUpdate"),
				user.getName(), user.getEmail(), user.getPassword(), user.getLevel().intValue(), user.getLogin(), user.getRecommend(), user.getId());
	}

}
