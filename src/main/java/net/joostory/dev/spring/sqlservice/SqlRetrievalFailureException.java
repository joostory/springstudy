package net.joostory.dev.spring.sqlservice;

public class SqlRetrievalFailureException extends RuntimeException {

	private static final long serialVersionUID = 2643341531927625645L;

	public SqlRetrievalFailureException(String message) {
		super(message);
	}
	
	public SqlRetrievalFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public SqlRetrievalFailureException(Throwable cause) {
		super(cause);
	}
	
}
