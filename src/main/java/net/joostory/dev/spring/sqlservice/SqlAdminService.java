package net.joostory.dev.spring.sqlservice;

public class SqlAdminService implements AdminEventListener {

	private static final String KEY_ID = null;
	private static final String SQL_ID = null;
	
	private UpdatableSqlRegistry updatableSqlRegistry;
	
	public void setUpdatableSqlRegistry(UpdatableSqlRegistry updatableSqlRegistry) {
		this.updatableSqlRegistry = updatableSqlRegistry;
	}


	@Override
	public void updateEventListner(UpdateEvent event) {
		updatableSqlRegistry.updateSql(event.get(KEY_ID), event.get(SQL_ID));
	}

}
