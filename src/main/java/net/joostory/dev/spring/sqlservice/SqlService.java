package net.joostory.dev.spring.sqlservice;

public interface SqlService {

	String getSql(String key) throws SqlRetrievalFailureException;
}
