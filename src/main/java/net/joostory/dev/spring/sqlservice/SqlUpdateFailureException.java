package net.joostory.dev.spring.sqlservice;

public class SqlUpdateFailureException extends RuntimeException {

	private static final long serialVersionUID = -3025166832419741035L;

	public SqlUpdateFailureException(String message) {
		super(message);
	}
	
	public SqlUpdateFailureException(String message, Throwable cause) {
		super(message, cause);
	}
}
