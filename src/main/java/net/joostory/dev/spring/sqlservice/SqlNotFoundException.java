package net.joostory.dev.spring.sqlservice;

public class SqlNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 6216617515214303023L;

	public SqlNotFoundException(String message) {
		super(message);
	}
	
	public SqlNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
