package net.joostory.dev.spring.sqlservice;

import java.util.HashMap;
import java.util.Map;

public class MyUpdatableSqlRegistry implements UpdatableSqlRegistry {

	private Map<String, String> sqlMap = new HashMap<String, String>();
	
	@Override
	public void registerSql(String key, String sql) {
		sqlMap.put(key, sql);
	}

	@Override
	public String findSql(String key) throws SqlNotFoundException {
		String sql = sqlMap.get(key);
		if (sql == null) {
			throw new SqlRetrievalFailureException("Cannot found key : " + key);
		} else {
			return sql;
		}
	}

	@Override
	public void updateSql(String key, String sql) throws SqlUpdateFailureException {
		this.sqlMap.put(key, sql);
	}

	@Override
	public void updateSql(Map<String, String> sqlMap) throws SqlUpdateFailureException {
		this.sqlMap.putAll(sqlMap);
	}

}
