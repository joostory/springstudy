package net.joostory.dev.spring.sqlservice;

public interface SqlReader {
	void read(SqlRegistry sqlRegistry);
}
