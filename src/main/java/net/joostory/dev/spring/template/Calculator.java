package net.joostory.dev.spring.template;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Calculator {

	public int calcsum(String path) throws IOException {
		LineCallback<Integer> sumCallback = new LineCallback<Integer>() {
			
			@Override
			public Integer doSomeThingWithLine(String line, Integer value) throws IOException {
				return value + Integer.valueOf(line);
			}

		};
		
		return lineReadTemplate(path, sumCallback, 0);
	}

	public int calcMultiply(String path) throws IOException {
		LineCallback<Integer> multiplyCallback = new LineCallback<Integer>() {

			@Override
			public Integer doSomeThingWithLine(String line, Integer value) throws IOException {
				return value * Integer.valueOf(line);
			}
			
		};
		
		return lineReadTemplate(path, multiplyCallback, 1);
	}

	public String concatenate(String path) throws IOException {
		LineCallback<String> concatenateCallback = new LineCallback<String>() {

			@Override
			public String doSomeThingWithLine(String line, String value) throws IOException {
				return value + line;
			}
			
		};
		
		return lineReadTemplate(path, concatenateCallback, "");
	}
	
	private <T> T lineReadTemplate(String path, LineCallback<T> callback, T initVal) throws IOException {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path));
			T res = initVal; 
			String line = null;
			while((line = br.readLine()) != null) {
				res = callback.doSomeThingWithLine(line, res);
			}
			return res;
		} catch(IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
