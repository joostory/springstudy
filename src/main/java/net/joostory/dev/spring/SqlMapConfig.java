package net.joostory.dev.spring;

import org.springframework.core.io.Resource;

public interface SqlMapConfig {
	Resource getSqlMapResource();
}
