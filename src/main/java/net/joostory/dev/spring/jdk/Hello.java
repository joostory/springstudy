package net.joostory.dev.spring.jdk;

public interface Hello {
	String sayHello(String name);
	String sayHi(String name);
	String sayThankYou(String name);
}
