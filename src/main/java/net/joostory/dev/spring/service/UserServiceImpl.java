package net.joostory.dev.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import net.joostory.dev.spring.dao.UserDao;
import net.joostory.dev.spring.domain.Level;
import net.joostory.dev.spring.domain.User;


@Component("userService")
public class UserServiceImpl implements UserService {
	public static final int MIN_RECOMMEND_FOR_GOLD = 30;
	public static final int MIN_LOGCOUNT_FOR_SILVER = 50;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private MailSender mailSender;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Override
	public void add(User user) {
		if (user.getLevel() == null) {
			user.setLevel(Level.BASIC);
		}
		userDao.add(user);
	}
	
	@Override
	public void upgradeLevels() {
		List<User> users = userDao.getAll();
		for(User user:users) {
			if (canUpgradeLevel(user)) {
				upgradeLevel(user);
			}
		}
		
	}

	protected void upgradeLevel(User user) {
		user.upgradeLevel();
		userDao.update(user);
		sendUpgradeEmail(user);
	}

	private void sendUpgradeEmail(User user) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom("useradmin@ksug.org");
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Upgrade");
		mailMessage.setText("You are upgraded to " + user.getLevel() + ".");
		mailSender.send(mailMessage);
	}

	private boolean canUpgradeLevel(User user) {
		Level currentLevel = user.getLevel();
		switch(currentLevel) {
		case BASIC:
			return user.getLogin() >= MIN_LOGCOUNT_FOR_SILVER;
		case SILVER:
			return user.getRecommend() >= MIN_RECOMMEND_FOR_GOLD;
		case GOLD:
			return false;
		default:
			throw new IllegalArgumentException("Unknown level : " + currentLevel);
		}
	}

	@Override
	public User get(String id) {
		return userDao.get(id);
	}

	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Override
	public void deleteAll() {
		userDao.deleteAll();
	}

	@Override
	public void update(User user) {
		userDao.update(user);
	}
	
}
