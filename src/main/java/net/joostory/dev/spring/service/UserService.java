package net.joostory.dev.spring.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import net.joostory.dev.spring.domain.User;

@Transactional
public interface UserService {
	void add(User user);
	void update(User user);
	void deleteAll();
	
	void upgradeLevels();

	@Transactional(readOnly=true)
	User get(String id);
	
	@Transactional(readOnly=true)
	List<User> getAll();
}
